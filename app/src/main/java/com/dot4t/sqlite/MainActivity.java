package com.dot4t.sqlite;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView list;
    ArrayList<String> array;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list = findViewById(R.id.list);
        array = new ArrayList<>();
        adapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_list_item_1, android.R.id.text1, array);
        list.setAdapter(adapter);

        SQLiteDatabase bd = openOrCreateDatabase("meubanco", MODE_PRIVATE, null);

        bd.execSQL("DROP TABLE IF EXISTS alunos");

        bd.execSQL("CREATE TABLE IF NOT EXISTS alunos (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nome VARCHAR )");

        bd.execSQL("INSERT INTO alunos(nome) VALUES ('Jorge')");
        bd.execSQL("INSERT INTO alunos(nome) VALUES ('Tamiris')");
        bd.execSQL("INSERT INTO alunos(nome) VALUES ('Rômulo')");

        Cursor cursor = bd.rawQuery("SELECT * FROM alunos", null);
        cursor.moveToFirst();
        do {
            array.add(cursor.getString(cursor.getColumnIndex("nome")));
        } while(cursor.moveToNext());

        bd.close();

        adapter.notifyDataSetChanged();
    }
}
